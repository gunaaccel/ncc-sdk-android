package com.agero.nccsdk.internal.notification;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.notification.model.NccNotification;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * Created by james hermida on 12/11/17.
 */

@RunWith(PowerMockRunner.class)
public class NccNotificationManagerTest {

    @Mock
    private Context mockContext;

    @Mock
    private DataManager mockDataManager;

    @Mock
    private Drawable mockDrawable;

    private @DrawableRes int drawableId = 962011;

    private NotificationManager notificationManager;

    @Before
    public void setUp() throws Exception {
        notificationManager = new NccNotificationManager(mockContext, mockDataManager);
    }

    @Test
    public void setNotification_invalid_notification_id() throws Exception {
        try {
            notificationManager.setNotification(0, drawableId, null, null);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
            verifyZeroInteractions(mockDataManager);
        }
    }

    @Test
    @PrepareForTest({ContextCompat.class})
    public void setNotification_invalid_drawable_id() throws Exception {
        try {
            PowerMockito.mockStatic(ContextCompat.class);
            PowerMockito.when(ContextCompat.class, "getDrawable", mockContext, drawableId).thenReturn(null);

            notificationManager.setNotification(1, drawableId, null, null);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
            verifyZeroInteractions(mockDataManager);
        }
    }

    @Test
    @PrepareForTest({ContextCompat.class})
    public void setNotification_null_empty_title() throws Exception {

        PowerMockito.mockStatic(ContextCompat.class);
        PowerMockito.when(ContextCompat.class, "getDrawable", mockContext, drawableId).thenReturn(mockDrawable);

        String title = null;
        try {
            notificationManager.setNotification(1, drawableId, title, null);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
            verifyZeroInteractions(mockDataManager);
        }

        title = "";
        try {
            notificationManager.setNotification(1, drawableId, title, null);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
            verifyZeroInteractions(mockDataManager);
        }
    }

    @Test
    @PrepareForTest({ContextCompat.class})
    public void setNotification_null_empty_text() throws Exception {
        PowerMockito.mockStatic(ContextCompat.class);
        PowerMockito.when(ContextCompat.class, "getDrawable", mockContext, drawableId).thenReturn(mockDrawable);

        String title = "title";
        String text = null;
        try {
            notificationManager.setNotification(1, drawableId, title, text);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
            verifyZeroInteractions(mockDataManager);
        }

        text = "";
        try {
            notificationManager.setNotification(1, drawableId, title, text);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
            verifyZeroInteractions(mockDataManager);
        }
    }

    @Test
    @PrepareForTest({ContextCompat.class})
    public void setNotification() throws Exception {
        PowerMockito.mockStatic(ContextCompat.class);
        PowerMockito.when(ContextCompat.class, "getDrawable", mockContext, drawableId).thenReturn(mockDrawable);

        int id = 1;
        String title = "title";
        String text = "text";

        notificationManager.setNotification(id, drawableId, title, text);

        verify(mockDataManager).saveNotification(any(NccNotification.class));
    }

    @Test
    public void getNotification() throws Exception {
        int id = 962011;
        notificationManager.getNotification(id);

        verify(mockDataManager).getNotification(String.valueOf(id));
    }
}
