package com.agero.nccsdk.ubi;

import android.content.Context;

import com.agero.nccsdk.internal.common.statemachine.StateMachine;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Created by james hermida on 10/19/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class UbiInactiveStateTest {

    private UbiInactiveState ubiInactiveState;

    @Mock
    private StateMachine<UbiState> mockUbiStateMachine;

    @Mock
    private Context mockContext;

    @Before
    public void setUp() throws Exception {
        ubiInactiveState = new UbiInactiveState(mockContext);
    }

    @Test
    public void startCollecting() {
        ubiInactiveState.startCollecting(mockUbiStateMachine);
        verify(mockUbiStateMachine).setState(any(UbiCollectionState.class));
    }

    @Test
    public void stopCollecting() {
        ubiInactiveState.stopCollecting(mockUbiStateMachine);
        verifyNoMoreInteractions(mockUbiStateMachine);
    }
}
