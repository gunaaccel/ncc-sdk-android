package com.agero.nccsdk.domain.config;

/**
 * Created by james hermida on 8/30/17.
 */

public class NccLinearAccelerometerConfig extends NccAbstractNativeConfig {

    public NccLinearAccelerometerConfig() {
        super();
    }

    public NccLinearAccelerometerConfig(int samplingRate) {
        super(samplingRate);
    }
}
