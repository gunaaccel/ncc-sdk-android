package com.agero.nccsdk.domain.data;

import com.google.android.gms.location.DetectedActivity;

/**
 * A data class representing motion activity.
 */

public class NccMotionData extends NccAbstractSensorData {

    @SuppressWarnings("unused")
    private static final String TAG = NccMotionData.class.getName();

    private final int activityType;
    private final int confidence;

    /**
     * Creates an instance of NccMotionData
     *
     * @param timestamp UTC timestamp in milliseconds
     * @param activityType The type of the activity
     * @param confidence The confidence percentage from 0 - 100 for the activity
     */
    public NccMotionData(long timestamp, int activityType, int confidence) {
        super(timestamp);

        this.activityType = activityType;
        this.confidence = confidence;
    }

    /**
     * Gets the type of the activity. See {@link DetectedActivity}.
     *
     * @return int of the type of the activity
     */
    @SuppressWarnings("unused")
    public int getActivityType() {
        return this.activityType;
    }

    /**
     * Gets the confidence of the activity
     *
     * @return int of the confidence percentage from 0 - 100 for the activity
     */
    @SuppressWarnings("unused")
    public int getConfidence() {
        return this.confidence;
    }

    /**
     * Gets the string representation of the activity type
     *
     * @return String of the activity
     */
    @SuppressWarnings("unused")
    public String getActivityString() {
        return getNameFromActivityType(this.activityType);
    }

    /**
     * Get the name of an activity type
     *
     * @param activityType The type of the activity
     *
     * @return name
     */
    private String getNameFromActivityType(int activityType) {

        switch (activityType) {

            case DetectedActivity.IN_VEHICLE:
                return "in_vehicle";

            case DetectedActivity.ON_BICYCLE:
                return "on_bicycle";

            case DetectedActivity.ON_FOOT:
                return "on_foot";

            case DetectedActivity.STILL:
                return "still";

            case DetectedActivity.UNKNOWN:
                return "unknown";

            case DetectedActivity.TILTING:
                return "tilting";

            case DetectedActivity.WALKING:
                return "walking";

            case DetectedActivity.RUNNING:
                return "running";

            default:
                return "unsupported";
        }

    }

    @Override
    public String toString() {
        return "NccMotionData{" +
                "activityType=" + activityType +
                ", confidence=" + confidence +
                ", timestamp=" + timestamp +
                '}';
    }
}
