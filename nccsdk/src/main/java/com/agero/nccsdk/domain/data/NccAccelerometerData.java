package com.agero.nccsdk.domain.data;

import java.util.Locale;

/**
 * Created by james hermida on 8/21/17.
 */

public class NccAccelerometerData extends NccAbstractSensorData {

    private final float x;
    private final float y;
    private final float z;

    /**
     * Creates an instance of NccAccelerometerData
     *
     * @param timestamp UTC timestamp in milliseconds
     * @param x The acceleration the phone experiences in the x direction
     * @param y The acceleration the phone experiences in the y direction
     * @param z The acceleration the phone experiences in the z direction
     */
    public NccAccelerometerData(long timestamp, float x, float y, float z) {
        super(timestamp);

        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Gets the acceleration reading in the x direction
     *
     * @return float in the x direction of the accelerometer
     */
    @SuppressWarnings("unused")
    public float getX() {
        return this.x;
    }

    /**
     * Gets the acceleration reading in the y direction
     *
     * @return float in the y direction of the accelerometer
     */
    @SuppressWarnings("unused")
    public float getY() {
        return this.y;
    }

    /**
     * Gets the acceleration reading in the z direction
     *
     * @return float in the z direction of the accelerometer
     */
    @SuppressWarnings("unused")
    public float getZ() {
        return this.z;
    }

    @Override
    public String toString() {
        return "NccAccelerometerData{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", timestamp=" + timestamp +
                '}';
    }
}
