package com.agero.nccsdk.domain.data;

import java.util.Locale;

/**
 * Created by james hermida on 8/24/17.
 */

public class NccGyroscopeUncalibratedData extends NccAbstractSensorData {

    private final float rotationX;
    private final float rotationY;
    private final float rotationZ;
    private final float driftX;
    private final float driftY;
    private final float driftZ;

    /**
     * Creates an instance of NccGyroscopeUncalibratedData
     *
     * @param timestamp UTC timestamp in milliseconds
     * @param rotationX The X-axis rotation rate in radians per second
     * @param rotationY The Y-axis rotation rate in radians per second
     * @param rotationZ The Z-axis rotation rate in radians per second
     * @param driftX This is the estimated drift around the x axis
     * @param driftY This is the estimated drift around the y axis
     * @param driftZ This is the estimated drift around the z axis
     */
    public NccGyroscopeUncalibratedData(long timestamp, float rotationX, float rotationY, float rotationZ, float driftX, float driftY, float driftZ) {
        super(timestamp);
        this.rotationX = rotationX;
        this.rotationY = rotationY;
        this.rotationZ = rotationZ;
        this.driftX = driftX;
        this.driftY = driftY;
        this.driftZ = driftZ;
    }

    /**
     * Gets the X-axis rotation rate in radians per second. The sign follows the right hand rule:
     * If the right hand is wrapped around the X axis such that the tip of the thumb points toward positive X,
     * a positive rotation is one toward the tips of the other four fingers.
     *
     * @return float of the X-axis rotation rate in radians per second
     */
    public float getRotationX() {
        return rotationX;
    }

    /**
     * Gets the Y-axis rotation rate in radians per second. The sign follows the right hand rule:
     * If the right hand is wrapped around the Y axis such that the tip of the thumb points toward positive Y,
     * a positive rotation is one toward the tips of the other four fingers.
     *
     * @return float of the Y-axis rotation rate in radians per second
     */
    public float getRotationY() {
        return rotationY;
    }

    /**
     * Gets the Z-axis rotation rate in radians per second. The sign follows the right hand rule:
     * If the right hand is wrapped around the Z axis such that the tip of the thumb points toward positive Z,
     * a positive rotation is one toward the tips of the other four fingers.
     *
     * @return float of the Z-axis rotation rate in radians per second
     */
    public float getRotationZ() {
        return rotationZ;
    }

    /**
     * Gets the estimated drift around the x axis
     *
     * @return float of the estimated drift around the x axis
     */
    public float getDriftX() {
        return driftX;
    }

    /**
     * Gets the estimated drift around the y axis
     *
     * @return float of the estimated drift around the y axis
     */
    public float getDriftY() {
        return driftY;
    }

    /**
     * Gets the estimated drift around the z axis
     *
     * @return float of the estimated drift around the z axis
     */
    public float getDriftZ() {
        return driftZ;
    }

    @Override
    public String toString() {
        return "NccGyroscopeUncalibratedData{" +
                "rotationX=" + rotationX +
                ", rotationY=" + rotationY +
                ", rotationZ=" + rotationZ +
                ", driftX=" + driftX +
                ", driftY=" + driftY +
                ", driftZ=" + driftZ +
                ", timestamp=" + timestamp +
                '}';
    }
}
