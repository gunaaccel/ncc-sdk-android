package com.agero.nccsdk.domain.config;

import com.google.android.gms.location.LocationRequest;

/**
 * Created by james hermida on 8/30/17.
 */

public class NccLocationConfig extends NccAbstractConfig {

    public enum Priority {

        HIGH_ACCURACY(LocationRequest.PRIORITY_HIGH_ACCURACY),
        BALANCED_POWER_ACCURACY(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY),
        LOW_POWER(LocationRequest.PRIORITY_LOW_POWER),
        NO_POWER(LocationRequest.PRIORITY_NO_POWER);

        private final int id;

        Priority(int id) {
            this.id = id;
        }

        int getId() {
            return id;
        }
    };

    // Defaults
    private int interval = 1000; // 1 sec
    private int fastestInterval = interval;
    private int priority = Priority.HIGH_ACCURACY.getId();

    public NccLocationConfig() {
        super();
    }

    public NccLocationConfig(int interval, int fastestInterval, Priority priority) {
        this.interval = interval;
        this.fastestInterval = fastestInterval;
        this.priority = priority.getId();
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public int getFastestInterval() {
        return fastestInterval;
    }

    public void setFastestInterval(int fastestInterval) {
        this.fastestInterval = fastestInterval;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority.getId();
    }
}
