package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccLocationData;
import com.agero.nccsdk.domain.data.NccSensorData;

/**
 * Created by james hermida on 10/27/17.
 */

public class NccLocationMapper extends AbstractSensorDataMapper {

    @Override
    public String map(NccSensorData data) {
        NccLocationData ld = (NccLocationData) data;
        clearStringBuilder();
        sb.append("G,");                                // Code
        sb.append(ld.getLatitude()); sb.append(",");    // Latitude
        sb.append(ld.getLongitude()); sb.append(",");   // Longitude
        sb.append(ld.getAltitude()); sb.append(",");    // Altitude
        sb.append(ld.getCourse()); sb.append(",");      // Course
        sb.append(ld.getAccuracy()); sb.append(",");    // HorizAccuracy
        sb.append("NA,");                               // VerticalAccuracy (not available on Android)
        sb.append(ld.getSpeed()); sb.append(",");       // Speed
        sb.append(ld.getDistance()); sb.append(",");    // Distance from last location
        sb.append("NA,");
        sb.append(ld.getTimestamp()); sb.append(",");   // UTCTime
        sb.append(getReadableTimestamp(ld.getTimestamp())); // Timestamp
        return sb.toString();
    }
}
