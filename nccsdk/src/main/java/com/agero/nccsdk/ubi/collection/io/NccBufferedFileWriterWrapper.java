package com.agero.nccsdk.ubi.collection.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Uses a {@link BufferedWriter} to buffer text before writing to file
 *
 * @see BufferedWriter
 */
public class NccBufferedFileWriterWrapper extends AbstractFileWriterWrapper {

    private final File file;

    public NccBufferedFileWriterWrapper(File file, Headers headers) throws IOException {
        super(new BufferedWriter(new FileWriter(file.getAbsolutePath())), headers);
        this.file = file;
    }

    @Override
    public File getFile() {
        return file;
    }
}
