package com.agero.nccsdk.lbt;

import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.adt.event.AdtEventListener;
import com.agero.nccsdk.internal.common.statemachine.AbstractStateMachine;

/**
 * Created by james hermida on 11/6/17.
 */

public class LbtStateMachine extends AbstractStateMachine<LbtState> implements AdtEventListener {

    public LbtStateMachine(LbtState state) {
        super(state);
    }

    public void addTrackingListener(NccSensorListener sensorListener) {
        currentState.addTrackingListener(this, sensorListener);
    }

    public void removeTrackingListener(NccSensorListener sensorListener) {
        currentState.removeTrackingListener(this, sensorListener);
    }

    @Override
    public void onDrivingStart() {
        currentState.onDrivingStart(this);
    }

    @Override
    public void onDrivingStop() {
        currentState.onDrivingStop(this);
    }
}
