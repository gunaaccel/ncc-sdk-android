package com.agero.nccsdk.lbt.network;

import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.data.NccSensorData;

import java.util.List;

/**
 * Created by james hermida on 9/19/17.
 */

public interface LbtSyncManager {

    void streamData(NccSensorType sensorType, List<? extends NccSensorData> sensorData);

}
