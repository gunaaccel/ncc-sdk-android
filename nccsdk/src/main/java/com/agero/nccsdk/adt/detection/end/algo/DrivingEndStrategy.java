package com.agero.nccsdk.adt.detection.end.algo;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.agero.nccsdk.adt.detection.ReleasableResources;
import com.agero.nccsdk.domain.data.NccSensorData;
import com.agero.nccsdk.adt.AdtTriggerType;
import com.agero.nccsdk.adt.detection.EvaluateStrategy;

import static com.agero.nccsdk.adt.AdtStateMachine.ACTION_TRIGGER_UPDATE;
import static com.agero.nccsdk.adt.AdtStateMachine.TRIGGER_TYPE;

/**
 * Created by james hermida on 11/14/17.
 */

public abstract class DrivingEndStrategy<T extends NccSensorData> implements EvaluateStrategy<T>, ReleasableResources {

    private final Context context;

    DrivingEndStrategy(Context context) {
        this.context = context;
    }

    void sendCollectionEndBroadcast(AdtTriggerType endTrigger) {
        Intent i = new Intent(ACTION_TRIGGER_UPDATE);
        i.putExtra(TRIGGER_TYPE, endTrigger);
        LocalBroadcastManager.getInstance(context).sendBroadcast(i);
    }
}
