package com.agero.nccsdk.adt.detection.start.algo;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;

import com.agero.nccsdk.adt.AdtTriggerType;
import com.agero.nccsdk.adt.detection.start.model.WifiActivity;
import com.agero.nccsdk.domain.data.NccWifiData;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.log.Timber;
import com.agero.nccsdk.adt.config.SessionConfig;

import static android.net.ConnectivityManager.TYPE_WIFI;

/**
 * Created by james hermida on 12/1/17.
 */

public class WifiDrivingStartStrategy extends DrivingStartStrategy<NccWifiData> implements Runnable {

    private final DataManager dataManager;
    private final SessionConfig sessionConfig;

    private HandlerThread handlerThread;
    private Handler handler;
    private boolean isStartSessionRunnablePosted = false;

    public WifiDrivingStartStrategy(Context context, SessionConfig sessionConfig, DataManager dataManager) {
        super(context);
        this.dataManager = dataManager;
        this.sessionConfig = sessionConfig;

        handlerThread = new HandlerThread(WifiDrivingStartStrategy.class.getSimpleName() + HandlerThread.class.getSimpleName());
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }

    @Override
    public void evaluate(NccWifiData networkData) {
        if (networkData == null) {
            Timber.e("Unable to evaluate network data for start strategy. data is null");
        } else {
            // Only handle Wifi network types
            if (networkData.getType() == TYPE_WIFI) {
                WifiActivity recentWifiActivity = dataManager.getRecentWifiActivity();

                if (networkData.isConnected()) {
                    // Connected
                    // Check if the device is connecting back to a recently disconnected wifi
                    // If so, then stop the wifi buffer service if the time since last disconnect
                    // is less than the reconnect threshold
                    if (recentWifiActivity.getDisconnection() != null) {
                        if (networkData.getDeviceName().equalsIgnoreCase(recentWifiActivity.getDisconnection().getSsid())
                                && networkData.getTimestamp() - recentWifiActivity.getDisconnection().getTimestamp()
                                        < sessionConfig.getWifiReconnectTimeThreshold()
                                && isStartSessionRunnablePosted) {
                            handler.removeCallbacks(this);
                            isStartSessionRunnablePosted = false;
                        }
                    }
                } else if (networkData.isConnectedOrConnecting()) {
                    // Connecting

                    // Do nothing here
                    // We check for this after isConnected() to avoid false positive connects
                    // and before !isConnected() to avoid false positive disconnects
                    // when the network is only connecting
                } else {
                    // Disconnected
                    if (!isStartSessionRunnablePosted) {
                        handler.postDelayed(this, sessionConfig.getWifiDisconnectStartSessionDelay());
                        isStartSessionRunnablePosted = true;
                    }
                }
            } else {
                String connectionState = networkData.isConnected() ? "connected" : networkData.isConnectedOrConnecting() ? "connecting" : "disconnected";
                Timber.w("Unexpected network event type: %s; isConnected: %s", networkData.getType(), connectionState);
            }
        }
    }

    @Override
    public void run() {
        sendCollectionStartBroadcast(AdtTriggerType.START_WIFI);
    }

    @Override
    public void releaseResources() {
        if (handler != null) {
            handler.removeCallbacks(this);
        }

        if (handlerThread != null && handlerThread.isAlive()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                Timber.v("Handler thread quit safely result: %s", handlerThread.quitSafely());
            } else {
                Timber.v("Handler thread quit result: %s", handlerThread.quit());
            }
        }
    }
}
