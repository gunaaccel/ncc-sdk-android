package com.agero.nccsdk.adt.detection.start.model;

/**
 * Created by james hermida on 12/1/17.
 */

public class WifiInfo {

    private String ssid;
    private long timestamp;

    public WifiInfo(String ssid, long timestamp) {
        this.ssid = ssid;
        this.timestamp = timestamp;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
