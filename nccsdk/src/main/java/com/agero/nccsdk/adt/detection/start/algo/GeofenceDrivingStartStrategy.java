package com.agero.nccsdk.adt.detection.start.algo;

import android.content.Context;
import android.location.Location;

import com.agero.nccsdk.adt.AdtTriggerType;
import com.agero.nccsdk.adt.detection.domain.geofence.GeofenceManager;
import com.agero.nccsdk.adt.detection.domain.geofence.model.NccGeofence;
import com.agero.nccsdk.domain.data.NccGeofenceEventData;
import com.agero.nccsdk.internal.log.Timber;
import com.google.android.gms.location.Geofence;

/**
 * Created by james hermida on 11/30/17.
 */

public class GeofenceDrivingStartStrategy extends DrivingStartStrategy<NccGeofenceEventData> {

    private final GeofenceManager geofenceManager;

    public GeofenceDrivingStartStrategy(Context context, GeofenceManager geofenceManager) {
        super(context);
        this.geofenceManager = geofenceManager;
    }

    @Override
    public void evaluate(NccGeofenceEventData event) {
        if (event == null) {
            Timber.e("Unable to evaluate geofence event for start strategy. event is null");
        } else if (event.getGeofenceTransition() == Geofence.GEOFENCE_TRANSITION_EXIT) {
            Location triggerLocation = event.getTriggeringLocation();
            NccGeofence geofence = geofenceManager.getGeofence();

            if (triggerLocation == null) {
                Timber.e("Unable to evaluate geofence event for start strategy. event trigger location is null");
            } else if (geofence == null) {
                Timber.e("Unable to evaluate geofence event for start strategy. geofence is null");
            } else {
                // Get the distance from the trigger location to the center of the geofence
                double triggerDistanceFromGeofence = calculateDistanceBetween(
                        geofence.getLatitude(), geofence.getLongitude(),
                        triggerLocation.getLatitude(), triggerLocation.getLongitude()
                );

                // The trigger distance from the geofence must be >= to the radius of the geofence
                if (triggerDistanceFromGeofence >= geofence.getRadius()) {
                    sendCollectionStartBroadcast(AdtTriggerType.START_GEOFENCE);
                } else {
                    Timber.w("Not starting session due to false positive geofence break. Trigger distance from geofence: %f meters", triggerDistanceFromGeofence);
                }
            }
        }
    }

    private double calculateDistanceBetween(double fromLatitude, double fromLongitude, double toLatitude, double toLongitude) {
        float[] distanceBetween = new float[3];
        Location.distanceBetween(fromLatitude, fromLongitude, toLatitude, toLongitude, distanceBetween);
        return distanceBetween[0];
    }

    @Override
    public void releaseResources() {

    }
}
