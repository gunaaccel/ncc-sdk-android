package com.agero.nccsdk;

import android.support.v4.util.ArrayMap;
import android.util.Log;

import java.util.Map;

/**
 * A mapping of String keys to various values which is included in location-based tracking data collected the SDK.
 */
public class TrackingContext {

    private final String TAG = TrackingContext.class.getSimpleName();

    private final Map<String, Object> map;

    public TrackingContext() {
        this.map = new ArrayMap<>();
    }

    /**
     * Returns a map of the key/value pairs of this TrackingContext
     *
     * @return a {@link Map} object
     */
    public Map<String, Object> getData() {
        return map;
    }

    /**
     * Returns the value associated with the given key, or null if
     * no mapping of the desired type exists for the given key.
     *
     * @param key a String
     * @return a string value or null
     */
    public String getString(String key) {
        Object o = map.get(key);
        if (o == null) {
            return null;
        } else {
            try {
                return (String) o;
            } catch (ClassCastException cce) {
                typeWarning(key, o, "String", cce);
                return null;
            }
        }
    }

    /**
     * Inserts a String value into the mapping of this TrackingContext, replacing
     * any existing value for the given key.
     *
     * @param key a String, or null
     * @param string a string
     */
    public void putString(String key, String string) {
        map.put(key, string);
    }

    /**
     * Returns the value associated with the given key, or 0 if
     * no mapping of the desired type exists for the given key.
     *
     * @param key a String
     * @return a double value
     */
    public double getDouble(String key) {
        Object o = map.get(key);
        if (o == null) {
            return 0;
        } else {
            try {
                return (Double) o;
            } catch (ClassCastException cce) {
                typeWarning(key, o, "Double", 0, cce);
                return 0;
            }
        }
    }

    /**
     * Inserts a double value into the mapping of this TrackingContext, replacing
     * any existing value for the given key.
     *
     * @param key a String, or null
     * @param d a string
     */
    public void putDouble(String key, double d) {
        map.put(key, d);
    }

    /**
     * Returns the value associated with the given key, or 0 if
     * no mapping of the desired type exists for the given key.
     *
     * @param key a String
     * @return an int value
     */
    public int getInt(String key) {
        Object o = map.get(key);
        if (o == null) {
            return 0;
        } else {
            try {
                return (Integer) o;
            } catch (ClassCastException cce) {
                typeWarning(key, o, "Integer", 0, cce);
                return 0;
            }
        }
    }

    /**
     * Inserts a int value into the mapping of this TrackingContext, replacing
     * any existing value for the given key.
     *
     * @param key a String, or null
     * @param i a string
     */
    public void putInt(String key, int i) {
        map.put(key, i);
    }

    /**
     * Returns the value associated with the given key, or 0 if
     * no mapping of the desired type exists for the given key.
     *
     * @param key a String
     * @return a long value
     */
    public long getLong(String key) {
        Object o = map.get(key);
        if (o == null) {
            return 0;
        } else {
            try {
                return (Long) o;
            } catch (ClassCastException cce) {
                typeWarning(key, o, "Long", 0, cce);
                return 0;
            }
        }
    }

    /**
     * Inserts a long value into the mapping of this TrackingContext, replacing
     * any existing value for the given key.
     *
     * @param key a String, or null
     * @param l a string
     */
    public void putLong(String key, long l) {
        map.put(key, l);
    }

    /**
     * Returns the value associated with the given key, or false if
     * no mapping of the desired type exists for the given key.
     *
     * @param key a String
     * @return a boolean value
     */
    public boolean getBoolean(String key) {
        Object o = map.get(key);
        if (o == null) {
            return false;
        } else {
            try {
                return (Boolean) o;
            } catch (ClassCastException cce) {
                typeWarning(key, o, "Boolean", false, cce);
                return false;
            }
        }
    }

    /**
     * Inserts a boolean value into the mapping of this TrackingContext, replacing
     * any existing value for the given key.
     *
     * @param key a String, or null
     * @param b a string
     */
    public void putBoolean(String key, boolean b) {
        map.put(key, b);
    }

    /**
     * Returns the value associated with the given key, or 0 if
     * no mapping of the desired type exists for the given key.
     *
     * @param key a String
     * @return a float value
     */
    public float getFloat(String key) {
        Object o = map.get(key);
        if (o == null) {
            return 0;
        } else {
            try {
                return (Float) o;
            } catch (ClassCastException cce) {
                typeWarning(key, o, "Float", 0, cce);
                return 0;
            }
        }
    }

    /**
     * Inserts a float value into the mapping of this TrackingContext, replacing
     * any existing value for the given key.
     *
     * @param key a String, or null
     * @param f a string
     */
    public void putFloat(String key, float f) {
        map.put(key, f);
    }

    // Log a message if the value was non-null but not of the expected type
    private void typeWarning(String key, Object value, String className,
                     Object defaultValue, ClassCastException e) {
        StringBuilder sb = new StringBuilder();
        sb.append("Key ");
        sb.append(key);
        sb.append(" expected ");
        sb.append(className);
        sb.append(" but value was a ");
        sb.append(value.getClass().getName());
        sb.append(".  The default value ");
        sb.append(defaultValue);
        sb.append(" was returned.");
        Log.w(TAG, sb.toString());
        Log.w(TAG, "Attempt to cast generated internal exception:", e);
    }

    private void typeWarning(String key, Object value, String className,
                     ClassCastException e) {
        typeWarning(key, value, className, "<null>", e);
    }
}
